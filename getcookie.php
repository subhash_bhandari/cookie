<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accessing cookiest</title>
</head>
<body>
    <?php
        //cookie values
        if(isset($_COOKIE['name'])){
            echo "cookie value is:",$_COOKIE['name'];
            echo "<br>";
        }else{
            echo "name is not set<br>";
        }
        if(isset($_COOKIE['age'])){
            echo "cookie value is:",$_COOKIE['age'];
        }else{
            echo "age is not set<br>";
        }
        if(isset($_COOKIE['cart'])){
            echo "<br>cookie value is:",$_COOKIE['cart'];
        }else{
            echo "cart is not set<br>";
        }
    ?>
    
</body>
</html>